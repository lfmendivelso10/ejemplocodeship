from django.test import TestCase
from models import Person
# Create your tests here.

class PersonTestCase(TestCase):
    """
    PersonTestCase has all tests of the Person Model for the app.
    """
    def setUp(self):
        """
            SetUp de Test Data
        """
        Person.objects.create(name="Tester", last_name="1")
        Person.objects.create(name="Tester", last_name="2")

    def test_full_name(self):
        """
        Test: Test Full Name
        Check the full name of a person
        """
        tester1 = Person.objects.get(id=1)
        tester2 = Person.objects.get(id=2)
        self.assertEqual(tester1.full_name(), 'Tester 1')
        self.assertEqual(tester2.full_name(), 'Tester 2')